import random
import os
import time

from datagrabber.datagrabber import datagrabber_os, datagrabber_sys
import matching
import display_options
import true_false

"""
DEPENDENCIES:
modules that should be installed for datagrabber module to work:
    - requests
    - lxml
    - cssselector

How to install:
    windows: >> py -3 -m pip install <name of module>
    linux: >> python3 -m pip install <name of module>

or install using a requirements.txt file for this project:
    py -3 -m pip install -r requirements.txt
    
"""

MENU = '''
1 for definitions
2 for multiple choice
3 for true/false
4 for matching
5 for quit

Please make your selection: '''

LIBRARY_MENU = '''
1 for os library
2 for sys library

Please make your selection: '''

# Pull in dictionary of modules specified by the requirements for function/method paired with their description
os_method_dict = datagrabber_os()
sys_method_dict = datagrabber_sys()
multiple_choice_list = ["A", "B", "C", "D"]

# Function that determines the system, and depending on
# the system, clears the console
def clear(): 
    _ = os.system('clear' if os.name =='posix' else 'cls') 

# Function takes in an input and a type and checks if the input can be cast as that type
def check_if_numeric(user_input, variable_kind):
    try:
        _ = variable_kind(user_input)
        return True
    except ValueError:
        return False

# Function that validates the input of a given prompt by calling the above function.
# If what is returned is false, the prompt simply repeats until a valid input is provided
def force_valid_input(prompt, variable_kind):
    valid_input = False
    while not valid_input:
        value = input(prompt)
        valid_input = check_if_numeric(value, variable_kind)
    value = variable_kind(value)
    return value

# Function that forces the user to make an actual input,
# and loops through until they do
def force_string_not_empty(prompt):
    empty = True
    while empty:
        user_input = input(prompt)
        if user_input.strip() != "":
            empty = False
        else:
            print("Input cannot be empty.")
    return user_input

# Function that gets user input to determine which display option they'd
# like to use to return definitions
def return_definition(method_dict):
    # Get choice from user
    choice = display_options.get_user_choice()
    # Run option the user picked
    if choice == 1:
        display_options.display_ten(os_method_dict)
    elif choice == 2:
        display_options.display_random(os_method_dict)

    print("Goodbye.")

# Function that randomly selects a method from the list and prints
# its definition as an answer as well as the definition of 3 other
# random methods, and verifies the user's selection as correct
def multiple_choice(method_dict):
    choice_list = []
    right_answer = random.choice(list(method_dict))
    choice_list.append(method_dict[right_answer])
    
    # Randomly selects 3 other unique definitions and appends them
    for _ in range(3):
        choice_key = random.choice(list(method_dict))
        while method_dict[choice_key] in choice_list:
            choice_key = random.choice(list(method_dict))
        choice_list.append(method_dict[choice_key])
    
    random.shuffle(choice_list)
    index = choice_list.index(method_dict[right_answer])

    print(f"What is the definition of {right_answer}?\n")
    print(f"A. {choice_list[0]}\n")
    print(f"B. {choice_list[1]}\n")
    print(f"C. {choice_list[2]}\n")
    print(f"D. {choice_list[3]}")

    user_answer = force_string_not_empty("Please make your selection: ").upper()
    # The user is informed if they are correct, or if they 
    # are incorrect, the correct answer is displayed to them
    if index == multiple_choice_list.index(user_answer):
        print("Correct!")
    else:
        print("Incorrect!")
        print(f"The correct answer was: {multiple_choice_list[index]}")


def true_false_game(method_list):
    chosen = []
    #all answers are false until proven true
    correct = False
    #initialize variables
    question = 1
    score = 0
    #11 can be changed to however many questions we deem appropriate
    #just cycles through the loop 10 times
    while question < 11:
        #print what question you're on
        print(question)
        matchpair = random.randint(0, 1)
        #select random pair of key and value
        if matchpair ==  0:
            #choose a random key
            method = random.choice(list(method_list))
            #grab a different method each time
            while method in chosen:
                method = random.choice(list(method_list))
            chosen.append(method)
            #print it
            print(method)
            #get a random value, using display_def function
            definition = true_false.display_def(method_list) 
            #if the key matches the value, set answer to true
            if definition == method_list.get(method):
                correct = True
        #select a method and its matching definition
        elif matchpair == 1:
            method = random.choice(list(method_list))
            while method in chosen:
                method = random.choice(list(method_list))
            chosen.append(method)
            print(method)
            definition = method_list.get(method)
            print(definition)
            correct = True

        #start the timer
        start = time.time()
        #prompt user response
        print()
        print("Does this method match this definition?")
        print("Type \"T\" for True or \"F\" for False")
        answer = force_string_not_empty(">>> ")

        #end the timer once user responds
        end = time.time()
        #see how many seconds have passed
        timepassed = end - start

        #evaluate answers, if the answer and the value for correct match
        if answer.lower() == 't' and correct == True:
            #add points earned based on time left to the total score
            score += true_false.points(timepassed)
            print("Correct".upper())
        elif answer.lower() == 'f' and correct == False:
            score += true_false.points(timepassed)
            print("Correct".upper())
        else:
            print("Incorrect".upper())
        #increase the question counter
        question += 1
        #reset the correct value
        correct = False 
        print()
    #at the end, let user know its the end of the game and display points
    #right now the most that can be scored is 5000
    print("End of game".upper())
    print(f"You scored {score} points")


def matching_game(method_dict):
    # Get number of matches from user
    num = matching.get_num_of_matches()
    # Create game matches
    matches_dict = matching.create_matches(num, method_dict)

    # Display definitions and possible answers,
    # and get list that holds the randomized order
    game_values = matching.display_matches(matches_dict)

    # Variable to hold number of incorrect and correct guesses
    incorrect_count = 0
    correct_count = 0

    # Get and check answers from user
    for num in range(0, len(matches_dict)):
        answer = matching.get_answer_choice(method_dict, num)
        incorrect_count, correct_count = matching.check_answer(matches_dict, game_values, answer, incorrect_count, correct_count, num)

    # Display number of incorrect and correct guesses
    print(f'\n\nInorrect answers: {incorrect_count}\n'
        f'Correct answers: {correct_count}')

    # Display the methods and their correct definitions
    matching.display_correct_answers(matches_dict)

# Function that takes in the user's menu 
# choice and calls the proper method
def call_proper_method(user_selection, method_dict):
    if user_selection == "1":
        print(return_definition(method_dict))
    elif user_selection == "2":
        multiple_choice(method_dict)
    elif user_selection == "3":
        true_false_game(method_dict)
    elif user_selection == "4":
        matching_game(method_dict)

# Main function that questions the user what they
# would like to do next until they quit out
def main():
    while True:
        menu_input = input(MENU)
        if menu_input == "5":
            quit()
        else:
            library_menu_input = input(LIBRARY_MENU)
            clear()
            if library_menu_input == "1":
                call_proper_method(menu_input, os_method_dict)
            elif library_menu_input == "2":
                call_proper_method(menu_input, sys_method_dict)

# Call to main function
main()
import requests
from lxml import html
from library_examples_processor import save_dictionary_pickle, load_dictionary_pickle

def return_parsed_html(help_url="https://docs.python.org/3.8/library/sys.html"):
    """
    This method will:
        - generate a request to pull data from a python.org module help page
        - parse the html and return that object.
         
    """
    data = requests.get(help_url)
    data.encoding = 'utf-8'
    html_tree = html.fromstring(data.text)
    return html_tree

def get_list_from_cssselector_from_html_tree(html_tree_object, cssselector_string):
    """
    Returns a list of child elements using a cssselector
    """
    return html_tree_object.cssselect(cssselector_string)

def get_string_content_from_element_obj(element_object):
    """
    Returns a string of the text content of the element object
    """

def generate_dict_from_python_web_element_list(element_list, dictionary_used={}):
    """
    This takes in a list of elements and will convert this list into a dictionary
    key: method element
    value: method description
    """
    for element in element_list:
        # a list of one is returned containing the function or data name
        element_key = get_list_from_cssselector_from_html_tree(element, 'dt')[0].text_content().strip()[:-1]
        element_description = " ".join([_.text_content().strip().replace('\n', ' ') for _ in get_list_from_cssselector_from_html_tree(element, 'dd')])
        dictionary_used[element_key] = element_description
    return dictionary_used

def print_the_dict(mydict):
    """
    Prints out the dictionary key value neatly

    """
    for key, value in mydict.items():
        print(f"""--------------
  Key: {key}

Value: 
    {value}""")

def datagrabber_sys():
    """
    This function checks for data types to grab and returns a dictionary of that data.
    """
    try:
        dict_to_return = datagrabber("https://docs.python.org/3/library/sys.html")
        # check to see if some valid information was returned, if not try to fall back on backup version of data
        assert len(dict_to_return) > 10
        try:
                # save information grabbed for future use if needed
            save_dictionary_pickle(dict_to_return, 'sys_functions.pickle')
        except:
            print("There was an issue saving a pickle for this dictionary.")        
        finally:
            return dict_to_return

    except:
        print("The internet data grab for this library is not functioning, retreiving fall back version.")
        return load_dictionary_pickle('sys_functions.pickle')

def datagrabber_os():
    """
    This function checks for data types to grab and returns a dictionary of that data.
    """
    try:

        dict_to_return = datagrabber("https://docs.python.org/3/library/os.html")
        # check to see if some valid information was returned, if not try to fall back on backup version of data
        assert len(dict_to_return) > 10
        try:
            # save information grabbed for future use if needed
            save_dictionary_pickle(dict_to_return, 'os_functions.pickle')
        except:
            print("There was an issue saving a pickle for this dictionary.")        
        finally:
            return dict_to_return

    except:
        print("The internet data grab for this library is not functioning, retreiving fall back version.")
        return load_dictionary_pickle('os_functions.pickle')

def datagrabber(html_url):
    """
    This function checks for data types to grab and returns a dictionary of that data.
    https://docs.python.org/3/library/sys.html
    https://docs.python.org/3/library/os.html 
    """
    # Get data from the internet
    html_tree = return_parsed_html(html_url)

    # Get a list of functions for the module
    function_list = get_list_from_cssselector_from_html_tree(html_tree, 'dl.function')

    # Return a dictionary with key=name and data = description
    return generate_dict_from_python_web_element_list(function_list)


if __name__ == "__main__":
    # print(__name__)
    # sys_tree = return_parsed_html()
    # function_list_sys = get_list_from_cssselector_from_html_tree(sys_tree, 'dl.function')
    # test_dict = generate_dict_from_python_web_element_list(function_list_sys) 
    print_the_dict(datagrabber_sys())
    print_the_dict(datagrabber_os())
    




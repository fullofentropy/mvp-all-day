os_example_dict = {
"os.access(path, mode, *, dir_fd=None, effective_ids=False, follow_symlinks=True)": ''' 
os.access('Today.txt',os.R_OK)
True
''',
"os.chdir(path)": ''' 
os.chdir('C:\\Users\\lifei\\Desktop')
None
''',
"os.chflags(path, flags, *, follow_symlinks=True)": ''' 
os.chflags('Today.txt',os.SF_NOUNLINK)
''',
"os.close(fd)": ''' 
fd = os.open('Today.txt',os.O_RDWR)
os.close(fd)
''',
"os.closerange(fd_low,fd_high)": ''' 
fd = os.open( "Today.txt", os.O_RDWR)
os.write(fd, "Testing")
os.closerange( fd, fd)
''',
"os.dup(fd)": ''' 
fd = os.open( "Today.txt", os.O_RDWR)
d_fd = os.dup( fd )
os.write(d_fd, "Testing")
os.closerange( fd, d_fd)
''',
"os.fchmod(fd, mode)": ''' 
fd = os.open( "/tmp", os.O_RDONLY )
os.fchmod( fd, stat.S_IXGRP)
os.fchmod(fd, stat.S_IWOTH)
print "Changed mode successfully!!"
os.close( fd )
''',
"os.fchown(fd, uid, gid)": ''' 
fd = os.open( "/tmp", os.O_RDONLY )
os.fchown( fd, 100, -1)
os.fchown( fd, -1, 50)
print "Changed ownership successfully!!"
os.close( fd )
''',
"os.lchmod(path, mode)": ''' 
path = "/var/www/html/Today.txt"
fd = os.open( path, os.O_RDWR )
os.close( fd )
os.lchmod( path, stat.S_IXGRP)
os.lchmod("/tmp/Today.txt", stat.S_IWOTH)
''',
"os.link(src, dst, *, src_dir_fd=None, dst_dir_fd=None, follow_symlinks=True)": ''' 
path = "/var/www/html/Today.txt"
fd = os.open( path, os.O_RDWR )
os.close( fd )
dst = "/tmp/Today.txt"
os.link( path, dst)
''',
"os.listdir(path='.')": ''' 
path = "/var/www/html/"
dirs = os.listdir( path )
for file in dirs:
    print(file)
''',
"os.openpty()": ''' 
m,s = os.openpty()
print(m)
print(s)
s = os.ttyname(s)
print(m)
print(s)
''',
"os.pipe()": ''' 
os.pipe()
(3, 4)
''',
"os.read(fd, n)": ''' 
fd = os.open("f1.txt",os.O_RDWR)
ret = os.read(fd,12)
print(ret)
os.close(fd)
''',
"os.readlink(path, *, dir_fd=None)": ''' 
src = '/usr/bin/python'
dst = '/tmp/python'
os.symlink(src, dst)
path = os.readlink( dst )
print(path)
''',
"os.stat(path, *, dir_fd=None, follow_symlinks=True)": ''' 
statinfo = os.stat('a2.py')
print(statinfo)
''',
"os.symlink(src, dst, target_is_directory=False, *, dir_fd=None)": ''' 
src = '/usr/bin/python'
dst = '/tmp/python'
os.symlink(src, dst)
''',
"os.tmpfile()": ''' 
tmpfile = os.tmpfile()
tmpfile.write('Temporary newfile is here.....')
tmpfile.seek(0)
print(tmpfile.read())
tmpfile.close()
'''
}

sys_example_dict = {
"sys.argv": ''' 
print("Hello {}. Welcome to {} tutorial".format(sys.argv[1], sys.argv[2]))
''',
"sys.maxsize": ''' 
sys.maxsize
9223372036854775807
''',
"sys.path": ''' 
sys.path
['', 'C:\\python36\\Lib\\idlelib', 'C:\\python36\\python36.zip', 'C:\\python36\\DLLs', 'C:\\python36\\lib', 'C:\\python36', 'C:\\Users\\acer\\AppData\\Roaming\\Python\\Python36\\site-packages', 'C:\\python36\\lib\\site-packages']
''',
"sys.version": ''' 
sys.version
'3.7.0 (v3.7.0:f59c0932b4, Mar 28 2018, 17:00:18) [MSC v.1900 64 bit (AMD64)]'
''',
"sys.float_info": ''' 
sys.float_info.dig
15
''',
"sys.hexversion": ''' 
if sys.hexversion >= 0x010502F0:
    # use some advanced feature
    ...
else:
    # use an alternative implementation or warn the user
    ...
''',
"sys.platform": ''' 
if sys.platform.startswith('freebsd'):
    # FreeBSD-specific code here...
elif sys.platform.startswith('linux'):
    # Linux-specific code here...
'''
}
import tkinter as tk
from tkinter import messagebox
 

from datagrabber.datagrabber import datagrabber_os, datagrabber_sys
import random
import time
import examples

datagrab = datagrabber_os()

menu = tk.Tk()

def mainwindow():
    menu.title("Menu Options")
    menu.geometry("300x300")

    opening = tk.Label(menu, text = "MVP ALL DAY\nGroup Project")
    opening.place(x = 110, y = 5)

    direction = tk.Label(menu, text = "Please make a selection")
    direction.place(x = 90, y = 20)

    define = tk.Button(menu, text = "Definitions", width = 20, command = definitionswindow)
    define.place(x = 75, y = 50)

    mc = tk.Button(menu, text = "Multiple Choice", width = 20)
    mc.place(x = 75, y = 100)

    tf = tk.Button(menu, text = "True/False", width = 20, command = truefalsewindow)
    tf.place(x = 75, y = 150)

    match = tk.Button(menu, text = "Matching", width = 20)
    match.place(x = 75, y = 200)

    end = tk.Button(menu, text = "QUIT", width = 20, command = quitwindow)
    end.place(x = 75, y = 250)

    menu.mainloop()

def definitionswindow():
    defpage = tk.Toplevel(menu)
    defpage.geometry("300x400")
    defpage.title("Definitions")

    userinput = tk.StringVar()

    instruct = tk.Label(defpage, text = "Enter a method name")
    instruct.pack()

    entered = tk.Entry(defpage, textvariable = userinput, width = 29)
    entered.place(x = 50, y = 30)

    press = tk.Button(defpage, text = "Enter", height = 1, width = 6, command = lambda: getanswer(userinput.get(), defpage))
    press.place(x = 230, y = 25)

def getanswer(userinput, window):

    if userinput in datagrab:
        if userinput in examples.os_example_dict:
            methoddef = tk.Label(window, text = f"\n{userinput}:\n{datagrab[userinput]}\n\nExample:{examples.os_example_dict[userinput]}", wraplength = 190, justify = "left")
            methoddef.place(x = 50, y = 60)

        else:
            methoddef = tk.Label(window, text = f"\n{userinput}:\n{datagrab[userinput]}\n\nExample:\nNo example for this method", wraplength = 190, justify = "left")
            methoddef.place(x = 50, y = 60)

    else:
        methoddef = tk.Label(window, text = "\nMethod Not Found", font = ('calibre', 12, 'normal'))
        methoddef.place(x = 80, y = 60)


    another = tk.Button(window, text = "Enter new method", command = lambda: [window.destroy(), definitionswindow()])
    another.place(x = 180, y = 350)

def truefalsewindow():
    tfpage = tk.Toplevel(menu)
    tfpage.geometry("500x500")
    tfpage.title("True/False")
    
    matchpair = random.randint(0, 1)
    correct = False

    if matchpair == 0:
        methodpick = random.choice(list(datagrab))
        definition = datagrab[methodpick]
    else:
        methodpick = random.choice(list(datagrab))
        randommethod = random.choice(list(datagrab))
        definition = datagrab[randommethod]

    if datagrab.get(methodpick) == definition:
        correct = True

    method = tk.Label(tfpage, text = methodpick, font = ('calibre', 14, 'normal'), wraplength = 400, justify = "center")
    method.pack()

    methoddefinition = tk.Label(tfpage, text = f"\n\n{definition}", wraplength = 400, justify = "center")
    methoddefinition.pack()

    truebutton = tk.Button(tfpage, text = "True", width = 12, bg = "green", fg = "white", 
                            command = lambda: [tfpage.destroy(), messagebox.showinfo("True/False", "You chose: True\nCORRECT")] if correct == True else [tfpage.destroy(), messagebox.showinfo("True/False", "You chose: True\nINCORRECT")])
    truebutton.place(x = 140, y = 400)

    falsebutton = tk.Button(tfpage, text = "False", width = 12, bg = "red", fg = "white", 
                            command = lambda: [tfpage.destroy(), messagebox.showinfo("True/False", "You chose: False\nCORRECT")] if correct == False else [tfpage.destroy(), messagebox.showinfo("True/False", "You chose: False\nINCORRECT")])
    falsebutton.place(x = 280, y = 400)

def quitwindow():
    messagebox.showinfo("QUIT", "Goodbye")
    quit()

mainwindow()
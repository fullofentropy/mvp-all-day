### for datagrabber module
from datagrabber.datagrabber import datagrabber_os, datagrabber_sys, print_the_dict

print_the_dict(datagrabber_os())
print_the_dict(datagrabber_sys())

### for the os/sys dictionary load/save module

from library_examples_processor import get_sys_lib_dict_examples, get_os_lib_dict_examples, save_os_lib_dict_examples, save_sys_lib_dict_examples

print('EMPTY VALUES INITIALIZED')
os_test = {}
sys_test = {}
save_os_lib_dict_examples(os_test, 'os_test.pickle')
save_sys_lib_dict_examples(sys_test, 'sys_test.pickle')
print(get_os_lib_dict_examples('os_test.pickle'))
print(get_sys_lib_dict_examples('sys_test.pickle'))

print('ADD VALUES, SAVE THEN PRINT VALUES')
sys_test = get_sys_lib_dict_examples('sys_test.pickle')
sys_test['meow'] = "cat"
os_test = get_os_lib_dict_examples('os_test.pickle')
os_test['woof'] = "dog"
save_os_lib_dict_examples(os_test, 'os_test.pickle')
save_sys_lib_dict_examples(sys_test, 'sys_test.pickle')
print(get_os_lib_dict_examples('os_test.pickle'))
print(get_sys_lib_dict_examples('sys_test.pickle'))



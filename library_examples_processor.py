# the goal of this feature is to pull from the current dictionary 
# of functions with descriptions as a key, value pair and generate 
# another dictionary with the same key but the value will be
# a string showing an example usage of that function
import pickle


####  GENERAL FUNCTIONS ######

def display_dictionary_value(dictionary, key):
    """
    Returns a dictionary value of an example as a nice view
    """
    if key in dictionary.keys():
        print(f"""
    function: {key}
    Example: {dictionary[key]}
        """)
        return True
        
    else:
        print("An example function does not exist yet.")
        return None

def save_dictionary_pickle(dictionary, filename_string):
    """
    This will save the passed in dictionary as pickle data with the passed in filename_string for the filename
    """
    with open(filename_string, 'wb+') as file:
        pickle.dump(dictionary, file)


def load_dictionary_pickle(filename_string):
    """
    This will load the filename passed in and return a dictionary
    """
    with open(filename_string, 'rb') as file:
        data_to_return = pickle.load(file)
        if isinstance(data_to_return, dict):
            return data_to_return
        else:
            return None

def add_to_stored_dict(key, value):
    """
    Takes a key value pair, opens pickle dictioanry, adds/updates the key/value pair
    then saves and closes the dictionary file.
    """


####  OS FUNCTIONS ######

def get_os_lib_dict_examples(name='os.pickle'):
    """
    returns a dictionary of the saved data storing examples of OS library functions
    if it doesn't exist, an empty dictionary is returned.
    """
    try:
        return load_dictionary_pickle(name)
    except:
        print('VALUES WERE NOT ABLE TO BE LOADED, RETURNING EMPTY DICTIONARY')
        return {}

def save_os_lib_dict_examples(dictionary, name='os.pickle'):
    """
    returns a dictionary of the saved data storing examples of OS library functions
    if it doesn't exist, an empty dictionary is returned.
    """
    try:
        save_dictionary_pickle(dictionary, name)
    except:
        print("could not save dictionary, returning None, please check inputs or if file is in-use")
        return None


####  SYS  FUNCTIONS ######

def get_sys_lib_dict_examples(name='sys.pickle'):
    """
    returns a dictionary of the saved data storing examples of OS library functions
    if it doesn't exist, an empty dictionary is returned.
    """
    try:
        return load_dictionary_pickle(name)
    except:
        print('VALUES WERE NOT ABLE TO BE LOADED, RETURNING EMPTY DICTIONARY')
        return {}

def save_sys_lib_dict_examples(dictionary, name='sys.pickle'):
    """
    returns a dictionary of the saved data storing examples of OS library functions
    if it doesn't exist, an empty dictionary is returned.
    """
    try:
        save_dictionary_pickle(dictionary, name)
    except:
        print("could not save dictionary, returning None, please check inputs or if file is in-use")
        return None


if __name__ == "__main__":
    print(__name__)
    print('EMPTY VALUES INITIALIZED')
    os_test = {}
    sys_test = {}
    save_os_lib_dict_examples(os_test, 'os_test.pickle')
    save_sys_lib_dict_examples(sys_test, 'sys_test.pickle')
    print(get_os_lib_dict_examples('os_test.pickle'))
    print(get_sys_lib_dict_examples('sys_test.pickle'))

    print('ADD VALUES, SAVE THEN PRINT VALUES')
    sys_test = get_sys_lib_dict_examples('sys_test.pickle')
    sys_test['meow'] = "cat"
    os_test = get_os_lib_dict_examples('os_test.pickle')
    os_test['woof'] = "dog"
    save_os_lib_dict_examples(os_test, 'os_test.pickle')
    save_sys_lib_dict_examples(sys_test, 'sys_test.pickle')
    print(get_os_lib_dict_examples('os_test.pickle'))
    print(get_sys_lib_dict_examples('sys_test.pickle'))


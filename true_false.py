from datagrabber.datagrabber import datagrabber_os, datagrabber_sys

import random
import time

#dictionary to make the file still functional
#even without being added to group project file
datagrab = datagrabber_os()

"""temporary dictionary to test functionality"""
# datagrab = {
#     'Alabama': 'Montgomery',
#     'Alaska': 'Juneau',
#     'Arizona':'Phoenix',
#     'Arkansas':'Little Rock',
#     'California': 'Sacramento',
#     'Colorado':'Denver',
#     'Connecticut':'Hartford',
#     'Delaware':'Dover',
#     'Florida': 'Tallahassee',
#     'Georgia': 'Atlanta',
#     'Hawaii': 'Honolulu',
#     'Idaho': 'Boise',
#     'Illinios': 'Springfield',
#     'Indiana': 'Indianapolis',
#     'Iowa': 'Des Monies',
#     'Kansas': 'Topeka',
#     'Kentucky': 'Frankfort',
#     'Louisiana': 'Baton Rouge',
#     'Maine': 'Augusta',
#     'Maryland': 'Annapolis',
#     'Massachusetts': 'Boston'}

def true_false(datagrab):
    chosen = []
    #all answers are false until proven true
    correct = False
    #initialize variables
    question = 1
    score = 0
    #11 can be changed to however many questions we deem appropriate
    #just cycles through the loop 10 times
    while question < 11:
        #print what question you're on
        print(question)
        matchpair = random.randint(0, 1)
        #select random pair of key and value
        if matchpair ==  0:
            #choose a random key
            method = random.choice(list(datagrab))
            #grab a different method each time
            while method in chosen:
                method = random.choice(list(datagrab))
            chosen.append(method)
            #print it
            print(method)
            #get a random value, using display_def function
            definition = display_def(datagrab) 
            #if the key matches the value, set answer to true
            if definition == datagrab.get(method):
                correct = True
        #select a method and its matching definition
        elif matchpair == 1:
            method = random.choice(list(datagrab))
            while method in chosen:
                method = random.choice(list(datagrab))
            chosen.append(method)
            print(method)
            definition = datagrab.get(method)
            print(definition)
            correct = True

        #start the timer
        start = time.time()
        #prompt user response
        print()
        print("Does this method match this definition?")
        print("Type \"T\" for True or \"F\" for False")
        answer = input(">>> ")
        #end the timer once user responds
        end = time.time()
        #see how many seconds have passed
        timepassed = end - start

        #evaluate answers, if the answer and the value for correct match
        if answer.lower() == 't' and correct == True:
            #add points earned based on time left to the total score
            score += points(timepassed)
            print("Correct".upper())
        elif answer.lower() == 'f' and correct == False:
            score += points(timepassed)
            print("Correct".upper())
        else:
            print("Incorrect".upper())
        #increase the question counter
        question += 1
        #reset the correct value
        correct = False 
        print()
    #at the end, let user know its the end of the game and display points
    #right now the most that can be scored is 5000
    print("End of game".upper())
    print(f"You scored {score} points")

#function grabs a random dictionary value and displays int
def display_def(datagrab):
    chosen = []
    #choose a random method from datalist
    method = random.choice(list(datagrab))
    #grab different method definitions each time
    while method in chosen:
        method =  random.choice(list(datagrab))
    chosen.append(method)
    #get the value of the key that was chosen
    definition = datagrab.get(method)
    #print the result and return it
    print(definition)
    return definition

#function determines points earned based on time taken to answer question
#time to respond can be changed, this is just a placeholder
def points(seconds):
    total = 0
    #if you take more than 15secs to answer, no points
    if seconds >= 15:
        total = 0
    #if you take between 10 and 15 seconds, you get a third of the points
    elif seconds < 15 and seconds > 10:
        total = 167
    #if time is between 5 and 10 secs, you get 2/3 of the points
    elif seconds < 10 and seconds > 5:
        total = 333
    #if less then 5secs, you get all possible points for a question
    #for right now that is 500
    else:
        total = 500
    #return how many points are earned
    return total

if __name__ == "__main__":
    true_false(datagrab)
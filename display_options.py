from datagrabber.datagrabber import datagrabber_os, datagrabber_sys
import random
import examples

# The display_ten function will display the methods in the library 10 at a time.
# After displaying it will ask the user if they want to continue and display the options
def display_ten(method_dict):
    # Display intro
    print("Ten methods will show at a time. They will be listed in alphabetical order.\n")
    # Create variable to keep count of methods being printed
    count = 1
    to_continue = ''
    
    # Print methods ten at a time
    for key in method_dict.keys():
        print(key)
        if count % 10 == 0:
            # Ask user if they want to continue - 
            # show ten more methods, quit, or show a definition
            to_continue = input("\nTo show a definition - type the method name\nTo quit - type 'n'\n" +
                                "Anything else will continue to show ten more methods\n>>> ")
            
            # Show method definition
            if to_continue in method_dict:
                # Show example if there is one
                display_def_ex(to_continue, method_dict)
                # Call if_at_end
                if_at_end(method_dict, False)

        # When all the methods have been displayed
        elif count == len(method_dict):
            print("\nYou have reached the end.")
            # Ask user their option of continuing
            to_continue = input("To show a definition - type the method name\nAnything else will be to quit.\n>>> ")
        
            # Show method definition
            if to_continue in method_dict:
                # Show example if there is one
                display_def_ex(to_continue, method_dict)
                # Call if_at_end
                if_at_end(method_dict, True)

        # Quit the program
        if to_continue == 'n':
            print("Goodbye.")
            quit()

        # Increment counter
        count += 1

# The display_def_ex function will display a method, its definition and an example if there is one
def display_def_ex(name, method_dict):
    # Show example if there is one
    if name in examples.os_example_dict:
        print(f"\n{name}:\n{method_dict[name]}\n\nExample:\n{examples.os_example_dict[name]}\n")
    else:
        print(f"\n{name}:\n{method_dict[name]}\n\nExample:\nNo example for this method\n")

# The if_at_end function determines what continue options to display depending if all the methods have been displayed
def if_at_end(method_dict, at_end):
    # If all the methods have been displayed
    if at_end == True:
        # Ask user their option of continuing
        to_continue = input("\nDo you still want to continue?\nTo show a definition - type another method name\n" +
                            "Anything else will be to quit'\n>>> ")

    # If all methods have not been displays
    else:
        # Ask user their option of continuing
        to_continue = input("\nDo you still want to continue?\nTo show a definition - type another method name\n" +
                            "To quit - type 'n'\nAnything else will continue to show ten more methods\n>>> ")
    
    # Show definition
    if to_continue in method_dict:
        display_def_ex(to_continue, method_dict)

    # Quit the program
    if to_continue == 'n':
        print("Goodbye.")
        quit()

# The random_method function asks the user to enter a method to get the definition
# or if they press Enter it will give them a random method and its definition
def display_random(method_dict):
    # Get method from user
    user_input = input("Enter a method you would like the definition of, or press enter for a random method: ")
    
    # Display random method and definition
    if user_input == "":
        # Get random method to display
        random_method = random.choice(list(method_dict))
        # Show example if there is one
        display_def_ex(random_method, method_dict)
    
    # If user input a method
    else:
        # Display method and definition and example if there is one
        if user_input in method_dict:
            display_def_ex(user_input, method_dict)
        # Method was not found in method_dict
        else:
            print("Method not found")

    # Ask user if they want to replay this program
    to_continue = input("Again? Type 'y' for yes\nAnything else will be no.\n>>> ").lower()
    if to_continue == 'y':
        display_random(method_dict)

# The get_user_choice function will get what option the user wants to use to show methods and their definitions
def get_user_choice():
    try:
        # Get choice from user
        choice = int(input("Both choices will give you the option to enter in a method name and get the definition.\n" +
                            "1 - Display ten methods at a time\n2 - Display random\n>>> "))
        # Input validation
        if choice != 1 and choice != 2:
            print("That was not a valid option.\n")
            choice = get_user_choice()
    # Error handling
    except ValueError:
        print("That was not a valid option.\n")
        choice = get_user_choice()

    return choice

if __name__ == '__main__':
    # Grab dictionary
    os_method_dict = datagrabber_os()
    # Get choice from user
    choice = get_user_choice()
    # Run option the user picked
    if choice == 1:
        display_ten(os_method_dict)
    elif choice == 2:
        display_random(os_method_dict)

    print("Goodbye.")

import random
from datagrabber.datagrabber import datagrabber_os, datagrabber_sys

# Function takes in an input and a type and checks if the input can be cast as that type
def check_if_numeric(user_input, variable_kind):
    try:
        _ = variable_kind(user_input)
        return True
    except ValueError:
        print('Invalid input, please try again.')
        return False

# Function that validates the input of a given prompt by calling the above function.
# If what is returned is false, the prompt simply repeats until a valid input is provided
def force_valid_input(prompt, variable_kind):
    valid_input = False
    while not valid_input:
        value = input(prompt)
        valid_input = check_if_numeric(value, variable_kind)
    value = variable_kind(value)
    return value

# The get_num_of_matches function gets user input for how many matches to do
def get_num_of_matches():
    num = force_valid_input("Enter in the number of matches you'd like to do: ", int)
    return num

# The create_matches function creates a dictionary of randomly picked methods and their definitions
def create_matches(count, method_dict):
    # Create dict to hold randomly picked methods from method_dict
    match_dict = dict()
    # Create copy of method dict
    copy_dict = dict((k,v) for k,v in method_dict.items())
    
    # Fill match_dict with random methods and their definitions from method_dict
    for num in range(0, count):
        # Rnandomly pick key from copy_dict
        rand_key = random.choice(list(copy_dict))
        # Add random key and value to match_dict
        match_dict[rand_key] = method_dict[rand_key]
        # Delete the key and value from the copy_dict to not get repeat picks
        del copy_dict[rand_key]
    
    return match_dict

# The display_matches function displays the methods and definitions in random order
def display_matches(method_dict):
    # Create list of keys from method dict
    key_list = [k for k in method_dict.keys()]
    # Create list of values from method_dict
    value_list = [v for v in method_dict.values()]
    # Create a max count variable
    count = len(method_dict)

    # Create list to hold the randomized values
    game_list_values = []

    print('Definitions:\n')

    # Randomly display definitions
    for num in range(0, count):
        rand_val = random.randint(0, count-1)
        print(f'{num+1}. {value_list[rand_val]}\n\n')
        # Add to game_list_values
        game_list_values.append(value_list[rand_val])

        # Decrement count
        count -= 1
        # Delete item from value_list to not get repeat picks
        del value_list[rand_val]

    # Reinitialize the max count
    count = len(method_dict)

    print('Answer choices:')

    # Randomly display answer choices
    for num in range(0, count):
        rand_key = random.randint(0, count-1)
        print(key_list[rand_key])

        # Decrement count
        count -= 1
        # Delete item from key_list to not get repeat picks
        del key_list[rand_key]

    return game_list_values

# The get_answer_choice method gets an answer from user
def get_answer_choice(method_dict, count):
    # Get answer from user
    answer = input(f'\nYou answer choice for definition: {count+1}. >>> ')
    # Input validation
    if answer not in method_dict.keys():
        print('You did not enter a possible answer choice. Please try again.')
        answer = get_answer_choice(method_dict, count)
        
    return answer

# The check_answer function will display if the user guess was correct or incorrect
# and will keep count of how many incorrect and correct guesses
def check_answer(method_dict, values, answer, incorrect_count, correct_count, count):
    # Display if answer was correct or incorrect
    # then increment/decrement its counter
    if method_dict[answer] == values[count]:
        print("Correct!")
        correct_count += 1
    else:
        print("Incorrect.")
        incorrect_count += 1

    return incorrect_count, correct_count

# The display_correct_answers function will print all the correct methods and definitions
def display_correct_answers(method_dict):
    for k, v in method_dict.items():
        print(f'\n{k} :\n{v}\n\n')

if __name__ == '__main__':
    # Display intro
    print("Matching: OS Library")
    # Grab library
    method_desc_dict_os = datagrabber_os()
    # Get number of matches from user
    num = get_num_of_matches()
    # Create game matches
    matches_dict = create_matches(num, method_desc_dict_os)
    # Display definitions and possible answers,
    # and get list that holds the randomized order
    game_values = display_matches(matches_dict)

    # Variable to hold number of incorrect and correct guesses
    incorrect_count = 0
    correct_count = 0

    # Get and check answer from user
    for num in range(0, len(matches_dict)):
        answer = get_answer_choice(method_desc_dict_os, num)
        incorrect_count, correct_count = check_answer(matches_dict, game_values, answer, incorrect_count, correct_count, num)

    # Display number of incorrect and correct guesses
    print(f'\n\nInorrect answers: {incorrect_count}\n'
        f'Correct answers: {correct_count}')

    # Display the methods and their correct definitions
    display_correct_answers(matches_dict)